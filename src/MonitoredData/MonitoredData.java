package MonitoredData;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Locale;

public class MonitoredData {

    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private String activityLabel;
    private static final DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public MonitoredData(LocalDateTime startTime, LocalDateTime endTime, String activityLabel){

        this.activityLabel = activityLabel;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public String getActivityLabel() {
        return activityLabel;
    }

    public void setActivityLabel(String activityLabel) {
        this.activityLabel = activityLabel;
    }

    public Integer getStartDay(){

        return startTime.getDayOfMonth();
    }

    public Float getAccurateInMinutes(){

        Duration result = Duration.between(startTime, endTime);

        long seconds = result.get(ChronoUnit.SECONDS);
        float minutes = (int)(seconds / 60);
        seconds -= (int)minutes * 60;

        minutes += seconds / 60.0;

        return minutes;
    }

    public Integer getDurationInMinutes(){

        Duration result = Duration.between(startTime, endTime);

        long seconds = result.get(ChronoUnit.SECONDS);

        return (int)(seconds / 60);
    }

    public Integer getDuration(){

        Duration result = Duration.between(startTime, endTime);

        long seconds = result.get(ChronoUnit.SECONDS);

        return (int)seconds;
    }

    public LocalTime getStartClockTime(){

        return LocalTime.of(startTime.getHour(), startTime.getMinute(), startTime.getSecond());
    }

    public LocalTime getEndClockTime(){

        return LocalTime.of(endTime.getHour(), endTime.getMinute(), endTime.getSecond());
    }

    public Integer getEndDay(){

        return endTime.getDayOfMonth();
    }

    @Override
    public String toString() {
        return df.format(getStartTime()) + " " + df.format(getEndTime()) + " " + getActivityLabel();
    }
}
