package Tasks;

import java.io.BufferedWriter;
import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import FilePrinter.*;
import MonitoredData.*;

public class TaskThree {

    public static void printToFile(Map<String, Long> data){

        BufferedWriter writer = FilePrinter.openFile("Task_3.txt");

        data.keySet().forEach((i) -> FilePrinter.writeLineToFile(writer, i + ": " + data.get(i)));

        FilePrinter.closeFile(writer);
    }

    /**
     * <h1>Get a map representing the number of occurrences of each activity</h1>
     * @param stream the stream supplier
     * @return Map representing the number of occur
     */
    public static Map<String, Long> group(Supplier<Stream<MonitoredData>> stream){

         return stream.get().collect(Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.counting()));

    }
}
