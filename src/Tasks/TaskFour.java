package Tasks;

import FilePrinter.*;
import MonitoredData.*;
import java.io.BufferedWriter;
import java.io.File;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TaskFour {

    public static void printToFile(Map<Integer, Map<String, Long>> dataStart){

        BufferedWriter writer = FilePrinter.openFile("Task_4.txt");

        dataStart.keySet().forEach((day -> {

            FilePrinter.writeLineToFile(writer, "day: " + day + "[");

            dataStart.get(day).keySet().forEach((activity) ->{

                FilePrinter.writeLineToFile(writer, "activity: " + activity + ", occurrences: " + dataStart.get(day).get(activity));

            });

            FilePrinter.writeLineToFile(writer, "]");
        }));

        FilePrinter.closeFile(writer);
    }

    /**
     * @param stream the stream supplier
     * @return Map representing the number of occurrences of each activity in each day
     */
    public static Map<Integer, Map<String, Long>> groupByDay(Supplier<Stream<MonitoredData>> stream){

        Map<Integer, Map<String, Long>> dataStart = stream.get()
                                                          .filter((d) -> d.getStartDay() == d.getEndDay())//get map based on start time
                                                          .collect(Collectors.groupingBy(MonitoredData::getStartDay,
                                                                                         Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.counting())));

        Map<Integer, Map<String, Long>> dataEnd = stream.get().filter((d) -> d.getStartDay() != d.getEndDay())//get map based on end time
                                                              .collect(Collectors.groupingBy(MonitoredData::getEndDay,
                                                                                             Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.counting())));


        dataEnd.keySet().forEach((dayEnd) ->{

            dataEnd.get(dayEnd).forEach((activity, occur) ->{

                if(dataStart.get(dayEnd).containsKey(activity))//update the occurrence if activity counted in end time map is found in the start time map
                    dataStart.get(dayEnd).replace(activity, dataStart.get(dayEnd).get(activity), dataStart.get(dayEnd).get(activity) + occur);
                else//activity not found in that day in start time map, new entry
                    dataStart.get(dayEnd).put(activity, occur);
            });
        });

        return dataStart;
    }
}
