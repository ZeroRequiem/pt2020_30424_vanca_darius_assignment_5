package Tasks;

import java.io.BufferedWriter;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import FilePrinter.*;
import MonitoredData.*;

public class TaskSix {

    public static void printToFile(List<String> resultData){

        BufferedWriter writer = FilePrinter.openFile("Task_6.txt");

        resultData.forEach((name) -> FilePrinter.writeLineToFile(writer, "name: " + name));

        FilePrinter.closeFile(writer);
    }

    /**
     *
     * @param stream the stream supplier
     * @return list of all the entries who have more than 90% of occurrences with a duration less than 5 minutes
     */
    public static List<String> getValid(Supplier<Stream<MonitoredData>> stream){

        Map<String, Long> allData = stream.get().collect(Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.counting()));//get number of occurrences of each activity

        Map<String, Long> validData = stream.get()//number of occurrences of activities which have a duration less than 5 minutes
                                            .filter((monitoredData) -> monitoredData.getAccurateInMinutes() < 5)
                                            .collect(Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.counting()));

        return allData.keySet().stream()
                               .filter((activity) -> {

                                   if(validData.containsKey(activity))//if the valid data contains the current activity
                                         return validData.get(activity) > 0.9 * allData.get(activity);//check if the number of occurrences is greater than 90%

                                   return false;//not found in valid data, automatically filtered out
                               })
                               .collect(Collectors.toList());//generate a list
    }
}
