package Tasks;

import java.io.BufferedWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import FilePrinter.*;
import MonitoredData.*;

public class TaskOne {

    /**
     * <h1>Generate a MonitoredData entry based on the details read from the lien</h1>
     * @param details the fields of the entry
     * @return The monitoredData entry
     */
    private static MonitoredData generateData(String[] details){

        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");//used for parsing

        try {

            return new MonitoredData(LocalDateTime.parse(details[0] + " " + details[1], df),//start time
                                     LocalDateTime.parse(details[2] + " " + details[3], df), //end time
                                     details[4]);//name of activity

        }catch (Exception e){
            e.printStackTrace();
        }

        return null;
    }

    /**
     * <h1>Used to split a line line into corresponding data</h1>
     * @param source the line
     * @return array of string which represents the details of the entry
     */
    private static String[] getDetails(String source){

        return source.split("\\s+");
    }

    /**
     * <h1>Static functions which returns the data read from the given file</h1>
     * @param path the path to the file
     * @return a list of the monitored data
     */
    public static List<MonitoredData> getData(String path){

        Path filePath = Paths.get(path);

        List<MonitoredData> data = null;
        BufferedWriter writer = FilePrinter.openFile("Task_1.txt");//open the file to write to

        try(Stream<String> lines = Files.lines(filePath)) {//put the lines in a stream

            data = lines.map((i) -> generateData(getDetails(i)))//map each line to a MonitoredData
                        .collect(Collectors.toList());//collect in a list

            data.forEach((i) -> FilePrinter.writeLineToFile(writer, i.toString()));//also print the result to a file to make sure everything is good

            FilePrinter.closeFile(writer);

            return data;

        }catch(Exception e){

            System.out.println(e.getMessage());
            System.exit(-1);
        }

        return null;
    }
}
