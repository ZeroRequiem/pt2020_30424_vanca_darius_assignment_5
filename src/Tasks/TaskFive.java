package Tasks;

import FilePrinter.*;
import CustomCollector.*;
import MonitoredData.*;
import java.io.BufferedWriter;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TaskFive {

    public static void printToFile(Map<String, Integer> data){

        BufferedWriter writer = FilePrinter.openFile("Task_5.txt");

        data.keySet().forEach((activity) ->{

            int seconds = data.get(activity);//this section is used to generate the actual time in days, hours, minutes and seconds
                                            //the total duration of an activity is kept as a number of seconds
            int minutes = seconds / 60;
            int hours = minutes / 60;
            int days = hours / 24;

            seconds -= minutes * 60;
            minutes -= hours * 60;
            hours -= days * 24;

            FilePrinter.writeLineToFile(writer, "activity: " + activity + ", duration:" + days + "d:" + hours + "h:" + minutes + "m:" + seconds + "s");
        });

        FilePrinter.closeFile(writer);
    }

    /**
     * @param stream stream supplier
     * @return map representing the total duration of each activity
     * a custom collector was used in order to simplify the process
     */
    public static Map<String, Integer> getDuration(Supplier<Stream<MonitoredData>> stream){

        return stream.get().collect(Collectors.groupingBy(MonitoredData::getActivityLabel, CustomCollector.duration()));

    }
}
