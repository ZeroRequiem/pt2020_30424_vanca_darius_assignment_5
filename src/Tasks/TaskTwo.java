package Tasks;

import java.io.BufferedWriter;
import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import FilePrinter.*;
import MonitoredData.*;

public class TaskTwo {

    public static void printToFile(Long count){

        BufferedWriter writer = FilePrinter.openFile("Task_2.txt");

        FilePrinter.writeLineToFile(writer, "distinct days: " + count);

        FilePrinter.closeFile(writer);
    }

    /**
     * <h1>Function used to get the number of distinct days</h1>
     * @param stream the stream supplier
     * @return the number of distinct days
     */
    public static Long countDays(Supplier<Stream<MonitoredData>> stream){

        return stream.get()
                     .map(MonitoredData::getStartDay)
                     .distinct()
                     .count();

    }
}
