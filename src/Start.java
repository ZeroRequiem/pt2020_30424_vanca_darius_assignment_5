import MonitoredData.MonitoredData;
import Tasks.*;

import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class Start {

    public static void main(String[] args) {

        if(args.length != 1){

            System.out.println("Invalid number of arguments");
            System.exit(-1);
        }

        List<MonitoredData> data =  TaskOne.getData(args[0]);

        Supplier<Stream<MonitoredData>> streamSupplier = data::stream;

        Long daysCounter = TaskTwo.countDays(streamSupplier);
        TaskTwo.printToFile(daysCounter);

        Map<String, Long> dataT3 = TaskThree.group(streamSupplier);
        TaskThree.printToFile(dataT3);

        Map<Integer, Map<String, Long>> dataT4 = TaskFour.groupByDay(streamSupplier);
        TaskFour.printToFile(dataT4);

        Map<String, Integer> dataT5 = TaskFive.getDuration(streamSupplier);
        TaskFive.printToFile(dataT5);

        List<String> dataT6 = TaskSix.getValid(streamSupplier);
        TaskSix.printToFile(dataT6);

    }
}
