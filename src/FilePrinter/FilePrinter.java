package FilePrinter;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class FilePrinter {

    public static BufferedWriter openFile(String name){

        try {

            return new BufferedWriter(new FileWriter(name));

        }catch (Exception e){

            e.printStackTrace();
            System.exit(-1);
        }

        return null;
    }

    public static void writeLineToFile(BufferedWriter writer, String content){

        try {

            writer.write(content + "\n");

        }catch (Exception e){

            e.printStackTrace();
            System.exit(-1);
        }
    }

    public static void writeToFile(BufferedWriter writer, String content){

        try {

            writer.write(content);

        }catch (Exception e){

            e.printStackTrace();
            System.exit(-1);
        }
    }

    public static void closeFile(BufferedWriter writer){

        try {

            writer.close();

        }catch (Exception e){

            e.printStackTrace();
            System.exit(-1);
        }
    }
}
