package CustomCollector;

import MonitoredData.MonitoredData;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;

public class CustomCollector {

    /**
     * <h1>A custom collector used for obtaining the total duration of an activity</h1>
     * @return a collector
     */
    public static Collector<MonitoredData, List<Integer>, Integer> duration() {

        return Collector.of(

                ArrayList::new,//supplier

                (result, monitoredData) -> {//accumulator

                    if ((result.size() == 0))
                        result.add(monitoredData.getDuration());
                    else
                        result.set(0, result.get(0) + monitoredData.getDuration());

                },

                (result1, result2) -> {//combiner

                    result1.set(0, result1.get(0) + result2.get(0));
                    return result1;

                },

                (result) -> result.get(0)//finisher

                );
    }
}
